'use strict';

const BTN = document.querySelector('.btn');
let infoWrapp = document.createElement('div');
BTN.after(infoWrapp);

async function catchUserIP(){
    let response = await fetch('https://api.ipify.org/?format=json')
    let userIP = await response.json()
    return userIP
}

async function getUserInfo(){
    let {ip} = await catchUserIP();
    let response = await fetch(`http://ip-api.com/json/${ip}`);
    let userInfo = await response.json()
    return userInfo;
}

BTN.addEventListener('click', async() => {
    let data = await getUserInfo();

    if(infoWrapp.firstChild){
        infoWrapp.removeChild(infoWrapp.firstChild)
    }

    let userInfoBlock = document.createElement('div');
    userInfoBlock.classList.add('user-info');
    infoWrapp.append(userInfoBlock);
    let userIP = document.createElement('span');
    userIP.innerText = `Your IP is: ${data.query}`
    userInfoBlock.append(userIP);
    let continent = document.createElement('span');
    continent.innerText = `Your continent is: ${data.timezone}`
    userInfoBlock.append(continent);
    let country = document.createElement('span');
    country.innerText = `Your country is: ${data.country}`
    userInfoBlock.append(country);
    let region = document.createElement('span');
    region.innerText = `Your region is: ${data.region}`
    userInfoBlock.append(region);
    let city = document.createElement('span');
    city.innerText = `Your city is: ${data.city}`
    userInfoBlock.append(city);
    let regionName = document.createElement('span');
    regionName.innerText = `Your region name is: ${data.regionName}`
    userInfoBlock.append(regionName);

    console.log('Done!')
})









